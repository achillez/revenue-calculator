<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>C</title>
    <meta http-equiv="content-language" content="en">

    <!-- for Google -->
    <meta name="description"
          content="Ad Insertion Revenue Calculator"/>
    <meta name="keywords" content=""/>

    <link rel="canonical" href="{{asset('logo.png')}}"/>


    <!-- for Facebook/Pinterest -->
    <meta property="og:title" content="Ad Insertion Revenue Calculator"/>
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="Ad Insertion Revenue Calculator"/>

    <meta property="og:image"
          content="{{asset('logo.png')}}png"/>

    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="630"/>
    <meta property="og:url" content="{{url('/')}}"/>
    <meta property="og:description"
          content="Ad Insertion Revenue Calculator"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="fb:app_id" content="327034044808557"/>

    <!-- for Twitter -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="Ad Insertion Revenue Calculator"/>
    <meta name="twitter:description"
          content="Ad Insertion Revenue Calculator"/>

    <meta name="twitter:image"
          content="{{asset('logo.png')}}"/>


    <meta name="robots" content="index, follow">

{{--    <link rel="icon" type="image/png" href="{{asset('favicon.ico')}}"/>--}}
    <link rel="stylesheet" type="text/css" href="{{asset('assets/bootstrap.css')}}"
          media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/style.css')}}"/>
    {{--    <link rel="stylesheet" type="text/css"--}}
    {{--          href="{{asset('assets/jquery-eu-cookie-law-popup.min.css')}}"/>--}}

    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lora:300,400,600,700&amp;lang=en"/>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:300,400,600,700&amp;lang=en"/>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,600,700&amp;lang=en"/>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Nunito"/>

    <style>

        @font-face {
            font-family: 'univia_pro';
            src: url('{{asset('assets')}}/fonts/univiapro-light.eot');
            src: url('{{asset('assets')}}/fonts/univiapro-light.eot?#iefix') format('embedded-opentype'),
            url('{{asset('assets')}}/fonts/univiapro-light.woff2') format('woff2'),
            url('{{asset('assets')}}fonts/univiapro-light.woff') format('woff'),
            url('{{asset('assets')}}/fonts/univiapro-light.ttf') format('truetype'),
            url('{{asset('assets')}}/fonts/univiapro-light.svg#univia_prolight') format('svg');
            font-weight: 300;
            font-style: normal;
        }

        @media (min-width: 992px) {
            .navbar-expand-lg .navbar-nav .nav-link {
                padding-right: 0.7rem;
                padding-left: 0.7rem;
            }
        }

        body {
            /*  font-family:Nunito;
            */
        }

        .input-group-append {
            height: calc(2.25rem + 2px);
        }

        p, li {
            line-height: 32px;
            /*    font-family: Lora;
            */
            font-size: 18px;
            /*        font-size: 1.15rem;
                line-height: 1.6;*/
        }

        .jumbotron {
            background-color: #EBF4FD;
        }

        h2 {
            margin-top: 1em;
            font-weight: 300;
        }

        .earnings, .normal-font {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        }

        .col-item {
            text-align: left !important;
        }

        /* for small screens */
        @media screen and (max-width: 768px) {
            h1 {
                font-size: 1.7rem !important;
                font-weight: 300;
            }

            h2 {
                font-size: 1.6rem !important;
                font-weight: 400;
            }


            /* Style all font awesome icons */
            .share-fa {
                padding: 10px !important;
                font-size: 20px !important;
                width: 40px !important;
                /*  width: 50px;*/
                text-align: center;
                text-decoration: none;
                border-radius: 50%;
                margin-left: 5px;
                margin-right: 5px;
            }

            /* Add a hover effect if you want */
            .share-fa:hover {
                opacity: 0.7;
                text-align: center;
                text-decoration: none;
                color: white;
            }


        }

        /*@media screen and (max-width: 500px) {*/
        /*    .table th, .table td {*/
        /*        padding: 5px;*/
        /*    }*/
        /*}*/

        .bg-formula {
            background-color: #f7ed72 !important;
        }

        /* Style all font awesome icons */
        .share-fa {
            padding: 15px;
            font-size: 25px;
            width: 55px;
            /*  width: 50px;*/
            text-align: center;
            text-decoration: none;
            border-radius: 50%;
            margin-left: 5px;
            margin-right: 5px;
        }

        /* Add a hover effect if you want */
        .share-fa:hover {
            opacity: 0.7;
            text-align: center;
            text-decoration: none;
            color: white;
        }

        /* https://www.w3schools.com/howto/howto_css_social_media_buttons.asp Set a specific color for each brand */

        /* Facebook */
        .fa-facebook-f {
            background: #3B5998;
            color: white;
        }

        /* Twitter */
        .fa-twitter {
            background: #55ACEE;
            color: white;
        }

        .fa-linkedin-in {
            background: #007bb5;
            color: white;
        }

        .fa-whatsapp {
            background: #25D366;
            color: white;
        }

        .fa-at, .fa-envelope {
            background: #ff6600;
            color: white;
        }


        .none {
            display: none !important;
        }

        .x-container {
            max-width: 1200px;
        }

        . table-responsive {
            overflow-x: scroll;
        }

        th {
            text-align: left;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-44691043-7"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-44691043-7');
    </script>

</head>

<body class="eupopup eupopup-bottom eupopup-style-compact">

<!-- Static navbar -->
<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-top" style="background: #00a553">
    <div class="container x-container justify-content-center">
        <a class="navbar-brand" href="/" style="margin: 15px;">
{{--            Ad Insertion Revenue Calculator--}}
        </a>
    </div>
</nav>


<div class="container x-container">
    <!-- Example row of columns -->
    <div class="">
        <div class="d-flex flex-column flex-sm-row justify-content-center justify-content-sm-start mt-3">
            <div>
                <div style="width: 250px" class="m-auto mr-sm-3 mr-lg-5">
                    <img src="{{asset('logo.png')}}" style="max-width: 100%; width: 100%; height: auto;">
                </div>
            </div>
            <h1 class="text-center page-heading align-self-center m-sm-0 mt-3" style="font-family: univia_pro">
                Ad Insertion Revenue Calculator
            </h1>
        </div>

        <div class="d-flex flex-row justify-content-center row">
            <button class="btn mt-3 mb-3 mr-2 ml-2 btn-default cal-type btn-success" data-ref="#linear">Linear
                Monetization
            </button>
            <button class="btn mt-3 mb-3 mr-2 ml-2 btn-default cal-type" data-ref="#live">OTT Monetization - Live
            </button>
            <button class="btn mt-3 mb-3 mr-2 ml-2 btn-default cal-type" data-ref="#vod">OTT Monetization - VOD</button>
        </div>

        <div id="linear" class="d-flex flex-row justify-content-between mt-3 cal-data row">
            <div class="col-md-6">

                <form>
                    <div class="form-row">

                        <div class="form-group col-12 col-lg-6">
                            <label>Total N. of Subs </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputSubs" placeholder="Total N. of Subs"
                                       value="0">
                                <div class="input-group-append">
                                    {{--                                <span class="input-group-text">$</span>--}}
                                    <button class="btn btn-minus" type="button" id="subsMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="subsPlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-lg-6">
                            <label>TV Viewing hours/day/sub</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputViewing"
                                       placeholder="TV Viewing hours/day/sub"
                                       value="0">
                                <div class="input-group-append">
                                    {{--                                <span class="input-group-text">$</span>--}}
                                    <button class="btn btn-minus" type="button" id="viewingMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="viewingPlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputeCPM">Percentage of subs watching your channel/s</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="inputWatch"
                                   placeholder="Percentage of subs watching your channel/s"
                                   value="0">
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                                <button class="btn btn-minus" type="button" id="watchMinus">
                                    <i class="fas fa-minus"></i></button>
                                <button class="btn btn-plus" type="button" id="watchPlus">
                                    <i class="fas fa-plus"></i></button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Ads / hour on your channel/s</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="inputHour"
                                   placeholder="Ads / hour on your channel/s"
                                   value="0">
                            <div class="input-group-append">
                                {{--                                <span class="input-group-text">$</span>--}}
                                <button class="btn btn-minus" type="button" id="hourMinus">
                                    <i class="fas fa-minus"></i></button>
                                <button class="btn btn-plus" type="button" id="hourPlus">
                                    <i class="fas fa-plus"></i></button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Percentage of spots available for linear monetization </label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="inputMone" placeholder="Linear Mone"
                                   value="0">
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                                <button class="btn btn-minus" type="button" id="moneMinus">
                                    <i class="fas fa-minus"></i></button>
                                <button class="btn btn-plus" type="button" id="monePlus">
                                    <i class="fas fa-plus"></i></button>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-12 col-lg-6">
                            <label>CPM Average</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputCpm" placeholder="CPM Average"
                                       value="0">
                                <div class="input-group-append">
                                    <span class="input-group-text">$</span>
                                    <button class="btn btn-minus" type="button" id="cpmMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="cpmPlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-lg-6">
                            <label for="inputeCPM">Growth per year</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputGrowth" placeholder="Growth per year"
                                       value="0">
                                <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                    <button class="btn btn-minus" type="button" id="growthMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="growthPlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-md-6 text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="card-text"><h4 class="">Total daily watching hours </h4>
                            <p id="daily-watching" class="earnings">0</p></div>
                        <hr>
                        <div class="card-text">
                            <h4 class="">Monthly watching hours on your channel/s</h4>
                            <p id="monthly-watching" class="earnings">0</p>
                        </div>
                        <hr>
                        <div class="card-text">
                            <h4 class="">Monthly impressions available for monetization on your channel/s</h4>
                            <p id="channel-impressions" class="earnings mb-0">0</p>
                        </div>
                        <hr>
                        <div class="card-text">
                            <h4 class="">Monthly total new revenue impressions</h4>
                            <p id="revenue-impressions" class="earnings mb-0">0</p>
                        </div>
                        <hr>
                        <div class="card-text">
                            <h4 class="">Monthly Total New Revenue</h4>
                            <p id="Monthly-impressions" class="earnings mb-0">0$</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-5">
                <div class=" table-responsive">
                    <table class="table table-striped table-hover" id="overview-table1">
                        <thead>
                        <tr>
                            <th scope="col">Year 1 Revenue</th>
                            <th scope="col">Year 2 Revenue</th>
                            <th scope="col">Year 3 Revenue</th>
                            <th scope="col">Year 4 Revenue</th>
                            <th scope="col">Year 5 Revenue</th>
                            <th scope="col" style="background: #f0fff9">Total 5 Years Revenue</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="live" class="d-flex flex-row justify-content-between mt-3 none cal-data row">
            <div class="col-md-6">

                <form>
                    <div class="form-row">

                        <div class="form-group col-12 col-lg-6">
                            <label>Total Monthly Live stream plays </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputPlays"
                                       placeholder="Total Monthly Live stream plays"
                                       value="0">
                                <div class="input-group-append">
                                    {{--                                <span class="input-group-text">$</span>--}}
                                    <button class="btn btn-minus" type="button" id="playsMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="playsPlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-lg-6">
                            <label>Average session duration in minutes</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputAverage"
                                       placeholder="Average session duration in minutes"
                                       value="0">
                                <div class="input-group-append">
                                    {{--                                <span class="input-group-text">$</span>--}}
                                    <button class="btn btn-minus" type="button" id="averageMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="averagePlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-12 col-lg-6">
                            <label>Ads / hour on your channel/s </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputHours"
                                       placeholder="Ads / hour on your channel/s"
                                       value="0">
                                <div class="input-group-append">
                                    {{--                                <span class="input-group-text">$</span>--}}
                                    <button class="btn btn-minus" type="button" id="hoursMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="hoursPlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-lg-6">
                            <label>% of avails available for New Revenue</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputRevenue"
                                       placeholder="% of avails available for New Revenue"
                                       value="0">
                                <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                    <button class="btn btn-minus" type="button" id="revenueMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="revenuePlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-12 col-lg-6">
                            <label>CPM Average</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputCpmLive"
                                       placeholder="CPM Average"
                                       value="0">
                                <div class="input-group-append">
                                    <span class="input-group-text">$</span>
                                    <button class="btn btn-minus" type="button" id="cpmliveMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="cpmlivePlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-lg-6">
                            <label>Growth per year</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputGrowthLive"
                                       placeholder="Growth per year"
                                       value="0">
                                <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                    <button class="btn btn-minus" type="button" id="growthliveMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="growthlivePlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-md-6 text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="card-text"><h4 class="">Average impressions / session </h4>
                            <p id="average-impressions" class="earnings">0</p></div>
                        <hr>
                        <div class="card-text">
                            <h4 class="">Total monthly impressions</h4>
                            <p id="monthly-impressions" class="earnings">0</p>
                        </div>
                        <hr>
                        <div class="card-text">
                            <h4 class="">Monthly estimated revenue</h4>
                            <p id="monthly-revenue" class="earnings mb-0">0$</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-5">
                <div class=" table-responsive">
                    <table class="table table-striped table-hover" id="overview-table2">
                        <thead>
                        <tr>
                            <th scope="col">Year 1 Revenue</th>
                            <th scope="col">Year 2 Revenue</th>
                            <th scope="col">Year 3 Revenue</th>
                            <th scope="col">Year 4 Revenue</th>
                            <th scope="col">Year 5 Revenue</th>
                            <th scope="col" style="background: #f0fff9">Total 5 Years Revenue</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <div id="vod" class="d-flex flex-row justify-content-between mt-3 none cal-data row">
            <div class="col-md-6">

                <form>
                    <div class="form-row">

                        <div class="form-group col-12 col-lg-6">
                            <label>Total Monthly VOD plays </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputPlaysVod"
                                       placeholder="Total Monthly VOD plays"
                                       value="0">
                                <div class="input-group-append">
                                    {{--                                <span class="input-group-text">$</span>--}}
                                    <button class="btn btn-minus" type="button" id="playsvodMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="playsvodPlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-lg-6">
                            <label>Average session duration in minutes</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputSession"
                                       placeholder="Average session duration in minutes"
                                       value="0">
                                <div class="input-group-append">
                                    {{--                                <span class="input-group-text">$</span>--}}
                                    <button class="btn btn-minus" type="button" id="sessionMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="sessionPlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-12 col-lg-6">
                            <label>Ads / hour on your VOD assets </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputHourVod"
                                       placeholder="Ads / hour on your VOD assets"
                                       value="0">
                                <div class="input-group-append">
                                    {{--                                <span class="input-group-text">$</span>--}}
                                    <button class="btn btn-minus" type="button" id="hourvodMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="hourvodPlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-lg-6">
                            <label>CPM Average</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputCpmVod"
                                       placeholder="CPM Average"
                                       value="0">
                                <div class="input-group-append">
                                    <span class="input-group-text">$</span>
                                    <button class="btn btn-minus" type="button" id="cpmvodMinus">
                                        <i class="fas fa-minus"></i></button>
                                    <button class="btn btn-plus" type="button" id="cpmvodPlus">
                                        <i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputeCPM">Growth per year</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="inputGrowthVod"
                                   placeholder="Growth per year"
                                   value="0">
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                                <button class="btn btn-minus" type="button" id="growthvodMinus">
                                    <i class="fas fa-minus"></i></button>
                                <button class="btn btn-plus" type="button" id="growthvodPlus">
                                    <i class="fas fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-md-6 text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="card-text"><h4 class="">Average impressions / session</h4>
                            <p id="average-impressions-vod" class="earnings">0</p></div>
                        <hr>
                        <div class="card-text">
                            <h4 class="">Total monthly impressions</h4>
                            <p id="monthly-impressions-vod" class="earnings">0</p>
                        </div>
                        <hr>
                        <div class="card-text">
                            <h4 class="">Monthly estimated revenue</h4>
                            <p id="monthly-revenue-vod" class="earnings mb-0">0$</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-5">
                <div class=" table-responsive">
                    <table class="table table-striped table-hover" id="overview-table3">
                        <thead>
                        <tr>
                            <th scope="col">Year 1 Revenue</th>
                            <th scope="col">Year 2 Revenue</th>
                            <th scope="col">Year 3 Revenue</th>
                            <th scope="col">Year 4 Revenue</th>
                            <th scope="col">Year 5 Revenue</th>
                            <th scope="col" style="background: #f0fff9">Total 5 Years Revenue</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->

<script src="{{asset('assets/jquery-3.3.1.js')}}"></script>
<script src="{{asset('assets/bootstrap.bundle.js')}}"></script>
<script src="{{asset('assets/custom.js')}}"></script>
{{--<script src="{{asset('assets/jquery-eu-cookie-law-popup.min.js')}}"></script>--}}
<script id="dsq-count-scr" src="{{asset('assets/count.js')}}" async></script>


<script type="text/javascript">
    jQuery(function ($) {
        $(document).ready(function () {

            $(".cal-type").click(function () {
                $(".cal-type").removeClass('btn-success');
                $(this).addClass('btn-success');
                $(".cal-data").addClass('none');
                $($(this).attr('data-ref')).removeClass('none');
            })

            const formUSD1 = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                maximumFractionDigits: 0,
                useGrouping: true
            });

            const formUSD2 = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 2,
                useGrouping: true
            });
            const form0 = new Intl.NumberFormat('en-US', {
                maximumFractionDigits: 0,
                useGrouping: true
            });

            function calcEarnings(type = 1) {

                if (type == 1) {
                    var subs = $("#inputSubs").val();
                    var viewing = $("#inputViewing").val();
                    var watch = $("#inputWatch").val();
                    var hour = $("#inputHour").val();
                    var mone = $("#inputMone").val();
                    var cpm = $("#inputCpm").val();
                    var growth = $("#inputGrowth").val();


                    $("#inputSubs").removeClass("danger");
                    $("#inputViewing").removeClass("danger");
                    $("#inputWatch").removeClass("danger");
                    $("#inputHour").removeClass("danger");
                    $("#inputMone").removeClass("danger");
                    $("#inputCpm").removeClass("danger");
                    $("#inputGrowth").removeClass("danger");

                    var hasError = false;

                    if (isNaN(subs)) {
                        $("#inputVisitors").addClass("danger");
                        hasError = true;
                    } else if (subs < 0) {
                        $("#inputVisitors").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(viewing)) {
                        $("#inputViewing").addClass("danger");
                        hasError = true;
                    } else if (viewing < 0) {
                        $("#inputViewing").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(watch)) {
                        $("#inputWatch").addClass("danger");
                        hasError = true;
                    } else if (watch < 0) {
                        $("#inputWatch").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(hour)) {
                        $("#inputHour").addClass("danger");
                        hasError = true;
                    } else if (hour < 0) {
                        $("#inputHour").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(mone)) {
                        $("#inputMone").addClass("danger");
                        hasError = true;
                    } else if (mone < 0) {
                        $("#inputMone").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(cpm)) {
                        $("#inputeCPM").addClass("danger");
                        hasError = true;
                    } else if (cpm < 0) {
                        $("#inputeCPM").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(growth)) {
                        $("#inputGrowth").addClass("danger");
                        hasError = true;
                    } else if (growth < 0) {
                        $("#inputGrowth").addClass("danger");
                        hasError = true;
                    }

                    if (hasError) {
                        $("#daily-watching").text('Error invalid input!');
                        $("#monthly-watching").text('Error invalid input!');
                        $("#channel-impressions").text('Error invalid input!');
                        $("#revenue-impressions").text('Error invalid input!');
                        $("#Monthly-impressions").text('Error invalid input!');
                        return;
                    }

                    daily_watching = subs * viewing;
                    monthly_watching = daily_watching * (watch / 100) * 30;
                    channel_impressions = monthly_watching * hour;
                    revenue_impressions = channel_impressions * (mone / 100);
                    monthly_revenue = (revenue_impressions * cpm) / 1000;

                    $("#daily-watching").text(form0.format(daily_watching));
                    $("#monthly-watching").text(form0.format(monthly_watching));
                    $("#channel-impressions").text(form0.format(channel_impressions));
                    $("#revenue-impressions").text(form0.format(revenue_impressions));
                    $("#Monthly-impressions").text(formUSD2.format(monthly_revenue));

                    //table
                    $('#overview-table1 > tbody').empty();

                    var yearRevenue1 = monthly_revenue * 12;
                    var yearRevenue2 = yearRevenue1 * (1 + (growth / 100));
                    var yearRevenue3 = yearRevenue2 * (1 + (growth / 100));
                    var yearRevenue4 = yearRevenue3 * (1 + (growth / 100));
                    var yearRevenue5 = yearRevenue4 * (1 + (growth / 100));
                    var yearTotalRevenue = yearRevenue1 + yearRevenue2 + yearRevenue3 + yearRevenue4 + yearRevenue5;

                    $('#overview-table1 > tbody:last-child').append('<tr class="table-info"><td>' + formUSD1.format(yearRevenue1) + '</td><td>' + formUSD1.format(yearRevenue2) + '</td><td>' + formUSD1.format(yearRevenue3) + '</td><td>' + formUSD1.format(yearRevenue4) + '</td><td>' + formUSD1.format(yearRevenue5) + '</td><td style="background: #f0fff9; color: green">' + formUSD1.format(yearTotalRevenue) + '</td></tr>');


                } else if (type == 2) {
                    var plays = $("#inputPlays").val();
                    var average = $("#inputAverage").val();
                    var hours = $("#inputHours").val();
                    var revenue = $("#inputRevenue").val();
                    var cpmlive = $("#inputCpmLive").val();
                    var growth = $("#inputGrowthLive").val();


                    $("#inputPlays").removeClass("danger");
                    $("#inputAverage").removeClass("danger");
                    $("#inputHours").removeClass("danger");
                    $("#inputRevenue").removeClass("danger");
                    $("#inputCpmLive").removeClass("danger");
                    $("#inputGrowthLive").removeClass("danger");

                    var hasError = false;

                    if (isNaN(plays)) {
                        $("#inputPlays").addClass("danger");
                        hasError = true;
                    } else if (plays < 0) {
                        $("#inputPlays").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(average)) {
                        $("#inputAverage").addClass("danger");
                        hasError = true;
                    } else if (average < 0) {
                        $("#inputAverage").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(hours)) {
                        $("#inputHours").addClass("danger");
                        hasError = true;
                    } else if (hours < 0) {
                        $("#inputHours").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(revenue)) {
                        $("#inputRevenue").addClass("danger");
                        hasError = true;
                    } else if (revenue < 0) {
                        $("#inputRevenue").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(cpmlive)) {
                        $("#inputCpmLive").addClass("danger");
                        hasError = true;
                    } else if (cpmlive < 0) {
                        $("#inputCpmLive").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(growth)) {
                        $("#inputGrowthLive").addClass("danger");
                        hasError = true;
                    } else if (growth < 0) {
                        $("#inputGrowthLive").addClass("danger");
                        hasError = true;
                    }

                    if (hasError) {
                        $("#average-impressions").text('Error invalid input!');
                        $("#monthly-impressions").text('Error invalid input!');
                        $("#monthly-revenue").text('Error invalid input!');
                        return;
                    }

                    average_impressions = (average / 60) * hours;
                    monthly_impressions = plays * average_impressions;
                    monthly_revenue = (monthly_impressions * cpmlive * (revenue / 100)) / 1000;

                    $("#average-impressions").text(form0.format(average_impressions));
                    $("#monthly-impressions").text(form0.format(monthly_impressions));
                    $("#monthly-revenue").text(form0.format(monthly_revenue));

                    //table
                    $('#overview-table2 > tbody').empty();

                    var yearRevenue1 = monthly_revenue * 12;
                    var yearRevenue2 = yearRevenue1 * (1 + (growth / 100));
                    var yearRevenue3 = yearRevenue2 * (1 + (growth / 100));
                    var yearRevenue4 = yearRevenue3 * (1 + (growth / 100));
                    var yearRevenue5 = yearRevenue4 * (1 + (growth / 100));
                    var yearTotalRevenue = yearRevenue1 + yearRevenue2 + yearRevenue3 + yearRevenue4 + yearRevenue5;

                    $('#overview-table2 > tbody:last-child').append('<tr class="table-info"><td>' + formUSD1.format(yearRevenue1) + '</td><td>' + formUSD1.format(yearRevenue2) + '</td><td>' + formUSD1.format(yearRevenue3) + '</td><td>' + formUSD1.format(yearRevenue4) + '</td><td>' + formUSD1.format(yearRevenue5) + '</td><td style="background: #f0fff9; color: green">' + formUSD1.format(yearTotalRevenue) + '</td></tr>');

                } else if (type == 3) {
                    var playsvod = $("#inputPlaysVod").val();
                    var session = $("#inputSession").val();
                    var hourvod = $("#inputHourVod").val();
                    var cpmvod = $("#inputCpmVod").val();
                    var growth = $("#inputGrowthVod").val();


                    $("#inputPlaysVod").removeClass("danger");
                    $("#inputSession").removeClass("danger");
                    $("#inputHourVod").removeClass("danger");
                    $("#inputCpmVod").removeClass("danger");
                    $("#inputGrowthVod").removeClass("danger");

                    var hasError = false;

                    if (isNaN(playsvod)) {
                        $("#inputPlaysVod").addClass("danger");
                        hasError = true;
                    } else if (playsvod < 0) {
                        $("#inputPlaysVod").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(session)) {
                        $("#inputSession").addClass("danger");
                        hasError = true;
                    } else if (session < 0) {
                        $("#inputSession").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(hourvod)) {
                        $("#inputHourVod").addClass("danger");
                        hasError = true;
                    } else if (hourvod < 0) {
                        $("#inputHourVod").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(cpmvod)) {
                        $("#inputCpmVod").addClass("danger");
                        hasError = true;
                    } else if (cpmvod < 0) {
                        $("#inputCpmVod").addClass("danger");
                        hasError = true;
                    }

                    if (isNaN(growth)) {
                        $("#inputGrowthVod").addClass("danger");
                        hasError = true;
                    } else if (growth < 0) {
                        $("#inputGrowthVod").addClass("danger");
                        hasError = true;
                    }

                    if (hasError) {
                        $("#average-impressions-vod").text('Error invalid input!');
                        $("#monthly-impressions-vod").text('Error invalid input!');
                        $("#monthly-revenue-vod").text('Error invalid input!');
                        return;
                    }

                    average_impressions_vod = (session / 60) * hourvod;
                    monthly_impressions_vod = playsvod * average_impressions_vod;
                    monthly_revenue = (monthly_impressions_vod * cpmvod) / 1000;

                    $("#average-impressions-vod").text(form0.format(average_impressions_vod));
                    $("#monthly-impressions-vod").text(form0.format(monthly_impressions_vod));
                    $("#monthly-revenue-vod").text(form0.format(monthly_revenue));

                    //table
                    $('#overview-table3 > tbody').empty();

                    var yearRevenue1 = monthly_revenue * 12;
                    var yearRevenue2 = yearRevenue1 * (1 + (growth / 100));
                    var yearRevenue3 = yearRevenue2 * (1 + (growth / 100));
                    var yearRevenue4 = yearRevenue3 * (1 + (growth / 100));
                    var yearRevenue5 = yearRevenue4 * (1 + (growth / 100));
                    var yearTotalRevenue = yearRevenue1 + yearRevenue2 + yearRevenue3 + yearRevenue4 + yearRevenue5;

                    $('#overview-table3 > tbody:last-child').append('<tr class="table-info"><td>' + formUSD1.format(yearRevenue1) + '</td><td>' + formUSD1.format(yearRevenue2) + '</td><td>' + formUSD1.format(yearRevenue3) + '</td><td>' + formUSD1.format(yearRevenue4) + '</td><td>' + formUSD1.format(yearRevenue5) + '</td><td style="background: #f0fff9; color: green">' + formUSD1.format(yearTotalRevenue) + '</td></tr>');

                }


            }

            calcEarnings(1);
            calcEarnings(2);
            calcEarnings(3);

            $("#inputSubs").on('keyup', function (e) {
                calcEarnings(1);
            });

            $("#inputViewing").on('keyup', function (e) {
                calcEarnings(1);
            });

            $("#inputWatch").on('keyup', function (e) {
                calcEarnings(1);
            });

            $("#inputHour").on('keyup', function (e) {
                calcEarnings(1);
            });

            $("#inputMone").on('keyup', function (e) {
                calcEarnings(1);
            });

            $("#inputCpm").on('keyup', function (e) {
                calcEarnings(1);
            });

            $("#inputGrowth").on('keyup', function (e) {
                calcEarnings(1);
            });


            $("#inputPlays").on('keyup', function (e) {
                calcEarnings(2);
            });

            $("#inputAverage").on('keyup', function (e) {
                calcEarnings(2);
            });

            $("#inputHours").on('keyup', function (e) {
                calcEarnings(2);
            });

            $("#inputRevenue").on('keyup', function (e) {
                calcEarnings(2);
            });

            $("#inputCpmLive").on('keyup', function (e) {
                calcEarnings(2);
            });

            $("#inputGrowthLive").on('keyup', function (e) {
                calcEarnings(2);
            });


            $("#inputPlaysVod").on('keyup', function (e) {
                calcEarnings(3);
            });

            $("#inputSession").on('keyup', function (e) {
                calcEarnings(3);
            });

            $("#inputHourVod").on('keyup', function (e) {
                calcEarnings(3);
            });

            $("#inputCpmVod").on('keyup', function (e) {
                calcEarnings(3);
            });

            $("#inputGrowthVod").on('keyup', function (e) {
                calcEarnings(3);
            });


            $("#subsPlus").click(function (e) {
                var value = $("#inputSubs").val();
                if (isNaN(value)) {
                    $("#inputSubs").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputSubs").val(value + 1);
                }
                calcEarnings(1);
            });
            $("#subsMinus").click(function (e) {
                var value = $("#inputSubs").val();
                if (isNaN(value)) {
                    $("#inputSubs").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputSubs").val(1);
                    } else if (value >= 2) {
                        $("#inputSubs").val(value - 1);
                    }
                }
                calcEarnings(1);
            });

            $("#viewingPlus").click(function (e) {
                var value = $("#inputViewing").val();
                if (isNaN(value)) {
                    $("#inputViewing").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputViewing").val(value + 1);
                }
                calcEarnings(1);
            });
            $("#viewingMinus").click(function (e) {
                var value = $("#inputViewing").val();
                if (isNaN(value)) {
                    $("#inputViewing").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputViewing").val(1);
                    } else if (value >= 2) {
                        $("#inputViewing").val(value - 1);
                    }
                }
                calcEarnings(1);
            });

            $("#watchPlus").click(function (e) {
                var value = $("#inputWatch").val();
                if (isNaN(value)) {
                    $("#inputWatch").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputWatch").val(value + 1);
                }
                calcEarnings(1);
            });
            $("#watchMinus").click(function (e) {
                var value = $("#inputWatch").val();
                if (isNaN(value)) {
                    $("#inputWatch").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputWatch").val(1);
                    } else if (value >= 2) {
                        $("#inputWatch").val(value - 1);
                    }
                }
                calcEarnings(1);
            });

            $("#hourPlus").click(function (e) {
                var value = $("#inputHour").val();
                if (isNaN(value)) {
                    $("#inputHour").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputHour").val(value + 1);
                }
                calcEarnings(1);
            });
            $("#hourMinus").click(function (e) {
                var value = $("#inputHour").val();
                if (isNaN(value)) {
                    $("#inputHour").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputHour").val(1);
                    } else if (value >= 2) {
                        $("#inputHour").val(value - 1);
                    }
                }
                calcEarnings(1);
            });

            $("#monePlus").click(function (e) {
                var value = $("#inputMone").val();
                if (isNaN(value)) {
                    $("#inputMone").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputMone").val(value + 1);
                }
                calcEarnings(1);
            });
            $("#moneMinus").click(function (e) {
                var value = $("#inputMone").val();
                if (isNaN(value)) {
                    $("#inputMone").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputMone").val(1);
                    } else if (value >= 2) {
                        $("#inputMone").val(value - 1);
                    }
                }
                calcEarnings(1);
            });

            $("#cpmPlus").click(function (e) {
                var value = $("#inputCpm").val();
                if (isNaN(value)) {
                    $("#inputCpm").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputCpm").val(value + 1);
                }
                calcEarnings(1);
            });
            $("#cpmMinus").click(function (e) {
                var value = $("#inputCpm").val();
                if (isNaN(value)) {
                    $("#inputCpm").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputCpm").val(1);
                    } else if (value >= 2) {
                        $("#inputCpm").val(value - 1);
                    }
                }
                calcEarnings(1);
            });

            $("#growthPlus").click(function (e) {
                var value = $("#inputGrowth").val();
                if (isNaN(value)) {
                    $("#inputGrowth").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputGrowth").val(value + 1);
                }
                calcEarnings(1);
            });
            $("#growthMinus").click(function (e) {
                var value = $("#inputGrowth").val();
                if (isNaN(value)) {
                    $("#inputGrowth").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputGrowth").val(1);
                    } else if (value >= 2) {
                        $("#inputGrowth").val(value - 1);
                    }
                }
                calcEarnings(1);
            });


            $("#playsPlus").click(function (e) {
                var value = $("#inputPlays").val();
                if (isNaN(value)) {
                    $("#inputPlays").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputPlays").val(value + 1);
                }
                calcEarnings(2);
            });
            $("#playsMinus").click(function (e) {
                var value = $("#inputPlays").val();
                if (isNaN(value)) {
                    $("#inputPlays").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputPlays").val(1);
                    } else if (value >= 2) {
                        $("#inputPlays").val(value - 1);
                    }
                }
                calcEarnings(2);
            });

            $("#averagePlus").click(function (e) {
                var value = $("#inputAverage").val();
                if (isNaN(value)) {
                    $("#inputAverage").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputAverage").val(value + 1);
                }
                calcEarnings(2);
            });
            $("#averageMinus").click(function (e) {
                var value = $("#inputAverage").val();
                if (isNaN(value)) {
                    $("#inputAverage").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputAverage").val(1);
                    } else if (value >= 2) {
                        $("#inputAverage").val(value - 1);
                    }
                }
                calcEarnings(2);
            });

            $("#hoursPlus").click(function (e) {
                var value = $("#inputHours").val();
                if (isNaN(value)) {
                    $("#inputHours").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputHours").val(value + 1);
                }
                calcEarnings(2);
            });
            $("#hoursMinus").click(function (e) {
                var value = $("#inputWatch").val();
                if (isNaN(value)) {
                    $("#inputHours").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputHours").val(1);
                    } else if (value >= 2) {
                        $("#inputHours").val(value - 1);
                    }
                }
                calcEarnings(2);
            });

            $("#revenuePlus").click(function (e) {
                var value = $("#inputRevenue").val();
                if (isNaN(value)) {
                    $("#inputRevenue").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputRevenue").val(value + 1);
                }
                calcEarnings(2);
            });
            $("#revenueMinus").click(function (e) {
                var value = $("#inputHour").val();
                if (isNaN(value)) {
                    $("#inputRevenue").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputRevenue").val(1);
                    } else if (value >= 2) {
                        $("#inputRevenue").val(value - 1);
                    }
                }
                calcEarnings(2);
            });

            $("#cpmlivePlus").click(function (e) {
                var value = $("#inputCpmLive").val();
                if (isNaN(value)) {
                    $("#inputCpmLive").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputCpmLive").val(value + 1);
                }
                calcEarnings(2);
            });
            $("#cpmliveMinus").click(function (e) {
                var value = $("#inputCpmLive").val();
                if (isNaN(value)) {
                    $("#inputCpmLive").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputCpmLive").val(1);
                    } else if (value >= 2) {
                        $("#inputCpmLive").val(value - 1);
                    }
                }
                calcEarnings(2);
            });

            $("#growthlivePlus").click(function (e) {
                var value = $("#inputGrowthLive").val();
                if (isNaN(value)) {
                    $("#inputGrowthLive").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputGrowthLive").val(value + 1);
                }
                calcEarnings(2);
            });
            $("#growthliveMinus").click(function (e) {
                var value = $("#inputGrowthLive").val();
                if (isNaN(value)) {
                    $("#inputGrowthLive").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputGrowthLive").val(1);
                    } else if (value >= 2) {
                        $("#inputGrowthLive").val(value - 1);
                    }
                }
                calcEarnings(2);
            });


            $("#playsvodPlus").click(function (e) {
                var value = $("#inputPlaysVod").val();
                if (isNaN(value)) {
                    $("#inputPlaysVod").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputPlaysVod").val(value + 1);
                }
                calcEarnings(3);
            });
            $("#playsvodMinus").click(function (e) {
                var value = $("#inputPlaysVod").val();
                if (isNaN(value)) {
                    $("#inputPlaysVod").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputPlaysVod").val(1);
                    } else if (value >= 2) {
                        $("#inputPlaysVod").val(value - 1);
                    }
                }
                calcEarnings(3);
            });

            $("#sessionPlus").click(function (e) {
                var value = $("#inputSession").val();
                if (isNaN(value)) {
                    $("#inputSession").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputSession").val(value + 1);
                }
                calcEarnings(3);
            });
            $("#sessionMinus").click(function (e) {
                var value = $("#inputSession").val();
                if (isNaN(value)) {
                    $("#inputSession").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputSession").val(1);
                    } else if (value >= 2) {
                        $("#inputSession").val(value - 1);
                    }
                }
                calcEarnings(3);
            });

            $("#hourvodPlus").click(function (e) {
                var value = $("#inputHourVod").val();
                if (isNaN(value)) {
                    $("#inputHourVod").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputHourVod").val(value + 1);
                }
                calcEarnings(3);
            });
            $("#hourvodMinus").click(function (e) {
                var value = $("#inputHourVod").val();
                if (isNaN(value)) {
                    $("#inputHourVod").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputHourVod").val(1);
                    } else if (value >= 2) {
                        $("#inputHourVod").val(value - 1);
                    }
                }
                calcEarnings(3);
            });

            $("#cpmvodPlus").click(function (e) {
                var value = $("#inputCpmVod").val();
                if (isNaN(value)) {
                    $("#inputCpmVod").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputCpmVod").val(value + 1);
                }
                calcEarnings(3);
            });
            $("#cpmvodMinus").click(function (e) {
                var value = $("#inputCpmVod").val();
                if (isNaN(value)) {
                    $("#inputCpmVod").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputCpmVod").val(1);
                    } else if (value >= 2) {
                        $("#inputCpmVod").val(value - 1);
                    }
                }
                calcEarnings(3);
            });

            $("#growthvodPlus").click(function (e) {
                var value = $("#inputGrowthVod").val();
                if (isNaN(value)) {
                    $("#inputGrowthVod").val(1);
                } else {
                    value = parseFloat(value);
                    $("#inputGrowthVod").val(value + 1);
                }
                calcEarnings(3);
            });
            $("#growthvodMinus").click(function (e) {
                var value = $("#inputGrowthVod").val();
                if (isNaN(value)) {
                    $("#inputGrowthVod").val(1);
                } else {
                    value = parseFloat(value);
                    if (value <= 1) {
                        $("#inputGrowthVod").val(1);
                    } else if (value >= 2) {
                        $("#inputGrowthVod").val(value - 1);
                    }
                }
                calcEarnings(3);
            });
        });
    });
</script>


</body>
</html>
