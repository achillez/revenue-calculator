@extends('layouts.app')

@section('title', trans('auth.unauthorized'))

@section('css')
    <link href="{{asset('assets/pages/css/error.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">

            <div class="row">
                <div class="col-md-12 page-500">
                    <div class=" number font-red"> 403</div>
                    <div class=" details">
                        <h3>@lang("common.oops").</h3>
                        <p> @lang("auth.unauthorized_action").
                            <br></p>
                        <p>
                            <a href="#" class="btn red btn-outline" id="back"
                               onclick="goBack()"> @lang("common.return_back") </a>
                            <br></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('js')
    <script>
        $("#back").click(function (e) {
            e.preventDefault();
            window.history.back();
        })
    </script>
@endsection