<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>{{config('app.name')}} | @lang('locked.lock_screen')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset(elixir('assets/app.css')) }}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/pages/css/lock.min.css')}}" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="{{asset('favicons/favicon.ico')}}"/>
        <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favicons')}}/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicons')}}/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="{{asset('favicons')}}/manifest.json">
        <link rel="mask-icon" href="{{asset('favicons')}}/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">

        <style>
            .login .copyright {
                color: #241212;
            }
        </style>
    </head>
    <!-- END HEAD -->

    <body>
        <div class="page-lock">
            <div class="page-logo">
                <a class="brand" href="{{ url(ADMINURL . '/login') }}">
                    <img src="{{asset('/logo1.png')}}" alt="@lang('common.logo')" /> </a>
            </div>
            <div class="page-body" style="margin:0">
                <div class="lock-head"> @lang('locked.locked') </div>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> @lang('locked.error') </span>
                </div>
                <div class="lock-body">
                    <div class="pull-left lock-avatar-block">
                        <img src="{{auth()->guard($guard)->user()->avatar_url}}" class="lock-avatar" style="width:110px"> </div>

                    <form class="lock-form pull-left" method="POST" action="{{ url('/locked') }}">
                        {{ csrf_field() }}
                        <h4>{{auth()->guard($guard)->user()->name}}</h4>
                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="@lang('locked.password')" name="password" /> 
                            @if ($errors->has('password'))
                            <span id="password-error" class="help-block">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn red uppercase">@lang('locked.login')</button>
                        </div>
                    </form>
                </div>
                <div class="lock-bottom">
                    <a href="{{url( ($guard == 'client' ? PORTALURL : ADMINURL) . '/logout')}}">@lang('locked.not') {{auth()->guard($guard)->user()->name}}?</a>
                </div>
            </div>
            <div class="page-footer-custom"> {{date('Y')}} © {{config('app.name')}}. {{config('app.footertext')}}. </div>
        </div>
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <script src="{{ asset(elixir('assets/app.js')) }}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript">
$(document).ready(function () {
    $('.lock-form').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            password: {
                required: true
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger').show();
        },
        highlight: function (element) { // hightlight error inputs
            $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.closest('.form-control'));
        },
        submitHandler: function (form) {
            form.submit(); // form validation success, call ajax form submit
        }
    });

    $('.lock-form input').keypress(function (e) {
        if (e.which == 13) {
            if ($('.lock-form').validate().form()) {
                $('.lock-form').submit(); //form validation success, call ajax form submit
            }
            return false;
        }
    });
    jQuery.extend(jQuery.validator.messages, {
        required: "@lang('common.field_required')",
        email: "@lang('common.email_valid')",
        equalTo: "@lang('common.equal_to')",
        extension: "@lang('common.ext_valid')"
    });
});
        </script>
    </body>
</html>