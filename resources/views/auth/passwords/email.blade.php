<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>@lang('login.elements') | @lang('login.forget_password')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset(elixir('assets/app.css')) }}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/pages/css/login-3.min.css')}}" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
        <style>
            .login .copyright {
                color: #241212;
            }
        </style>
    </head>
    <!-- END HEAD -->

    <body class=" login">

        <div class="menu-toggler sidebar-toggler"></div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="{{ url(ADMINURL . '/login') }}">
                <img src="{{asset('/logo1.png')}}" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" style="display: block" role="form" method="POST" action="{{ url(ADMINURL . '/password/email') }}">
                {{ csrf_field() }}
                <h3>@lang('login.forget_password')</h3>
                <p> @lang('login.enter_email') </p>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="@lang('login.email')" name="email" value="{{ old('email') }}"/> </div>
                    @if (session('status'))
                    <div class="alert alert-success">
                        <button class="close" data-close="alert"></button>
                        <span>
                            {{ session('status') }}
                        </span>
                    </div>
                    @endif

                    @if ($errors->has('email'))
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span>
                            {{ $errors->first('email') }}
                        </span>
                    </div>
                    @endif
                </div>
                <div class="form-actions">
                    <a href="{{ url(ADMINURL . '/login') }}" class="btn btn-default">@lang('login.back')</a>
                    <button type="submit" class="btn btn-success uppercase pull-right">@lang('login.submit')</button>
                </div>
            </form>

            <!-- END FORGOT PASSWORD FORM -->

        </div>
        <div class="copyright"> {{date('Y')}} © {{config('app.name')}}. {{config('app.footertext')}}. </div>
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <script src="{{ asset(elixir('assets/app.js')) }}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

        <script type="text/javascript">
$(document).ready(function () {
    $('.forget-form').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   

        },
        highlight: function (element) { // hightlight error inputs
            $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.closest('.input-icon'));
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    $('.forget-form input').keypress(function (e) {
        if (e.which == 13) {
            if ($('.forget-form').validate().form()) {
                $('.forget-form').submit();
            }
            return false;
        }
    });
    jQuery.extend(jQuery.validator.messages, {
        required: "@lang('common.field_required')",
        email: "@lang('common.email_valid')",
        equalTo: "@lang('common.equal_to')",
        extension: "@lang('common.ext_valid')"
    });
});
        </script>
    </body>
</html>