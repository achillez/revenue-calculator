<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>{{config('app.name')}} | @lang('login.user_login')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset(elixir('assets/app.css')) }}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/pages/css/login-3.min.css')}}" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="{{asset('favicons/favicon.ico')}}"/>
        <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favicons')}}/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicons')}}/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="{{asset('favicons')}}/manifest.json">
        <link rel="mask-icon" href="{{asset('favicons')}}/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">

        <style>
            .login .copyright {
                color: #241212;
            }
        </style>
    </head>
    <!-- END HEAD -->

    <body class=" login">
        <div class="menu-toggler sidebar-toggler"></div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <div class="logo" style="padding:0">
            <a href="{{ url(ADMINURL . '/login') }}">
                <img src="{{asset('/logo1.png')}}" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->

        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" role="form" method="POST" action="{{ url(ADMINURL . '/login') }}">
                {{ csrf_field() }}
                <h3 class="form-title">@lang('login.sign_in')</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> @lang('login.error') </span>
                </div>
                <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">@lang('login.username')</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="@lang('login.username')" name="username" value="{{ old('username') }}"/> </div>
                    @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">@lang('login.password')</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="@lang('login.password')" name="password" /> </div>
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">@lang('login.google_recaptcha')</label>
                    <!-- BEGIN REPCAPTCHA -->
                    <div class="g-recaptcha" data-theme="light" data-sitekey="6Lcd4CQTAAAAABWtV2hkWYy0jLPNplRy7F2w0v_v"></div>
                    @if ($errors->has('g-recaptcha-response'))
                    <span class="help-block">
                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                    </span>
                    @endif
                    <!-- END REPCAPTCHA -->
                    <!-- BEGIN GOOGLE RECAPTCHA -->
                    <style>
                        #rc-imageselect {transform:scale(1);-webkit-transform:scale(1);transform-origin:0 0;-webkit-transform-origin:0 0;}
                    </style>
                    <style>
                        @media (max-width: 480px){
                            #rc-imageselect, .g-recaptcha {transform:scale(0.73);-webkit-transform:scale(0.73);transform-origin:0 0;-webkit-transform-origin:0 0;}}
                        </style>
                    </div>
                    <div class="form-actions">
                    <div class="md-checkbox">
                        <input type="checkbox" id="remember" name="remember" value="1" class="md-check"/>
                        <label for="remember">
                            <span></span>
                            <span class="check"></span>
                            <span class="box"></span>@lang('login.remember')
                        </label>
                        <button type="submit" class="btn green pull-right"> @lang('login.login') </button>
                    </div>
                </div>
                <div class="forget-password">
                    <h4>@lang('login.forget_password')</h4>
                    <p> @lang('login.no_worries')
                        <a href="{{ url(ADMINURL . '/password/reset') }}" class="forget-password">@lang('login.forget_password')</a>
                    </p>
                </div>

            </form>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright"> {{date('Y')}} © {{config('app.name')}}. {{config('app.footertext')}}. </div>
        <!--[if lt IE 9]>
        <script src="../assets/global/plugins/respond.min.js"></script>
        <script src="../assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <script src="{{ asset(elixir('assets/app.js')) }}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script type="text/javascript">
$(document).ready(function () {
    $('.login-form').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            username: {
                required: true
            },
            password: {
                required: true
            },
            "g-recaptcha-response": {
                required: false
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (element) { // hightlight error inputs
            $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.closest('.input-icon'));
        },
        submitHandler: function (form) {
            form.submit(); // form validation success, call ajax form submit
        }
    });

    $('.login-form input').keypress(function (e) {
        if (e.which == 13) {
            if ($('.login-form').validate().form()) {
                $('.login-form').submit(); //form validation success, call ajax form submit
            }
            return false;
        }
    });
    jQuery.extend(jQuery.validator.messages, {
        required: "@lang('common.field_required')",
        email: "@lang('common.email_valid')",
        equalTo: "@lang('common.equal_to')",
        extension: "@lang('common.ext_valid')"
    });
});
        </script>
    </body>
</html>