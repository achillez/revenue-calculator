@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Dashboard</div>

        <div class="panel-body">
          You are logged in!
        </div>
      </div>
    </div>
  </div>
</div>

<!-- notifications -->
<script>
  var notification_submit = "{{url('/notifications/save-subscription')}}";
  var vapid_key = "{{env('VAPID_PUBLIC_KEY')}}";
</script>
<script src="{{asset('js/push-notifications.js')}}" type="text/javascript"></script>

@endsection

<!-- <div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Dashboard</div>

        <div class="panel-body">
          <button class="btn btn-info" id="enable-notifications" onclick="enableNotifications()"> Enable Push Notifications </button>
          <div class="form-group">
            <input class="form-control" id="title" placeholder="Notification Title">
          </div>
          <div class="form-group">
            <textarea id="body" class="form-control" placeholder="Notification body"></textarea>
          </div>
          <div class="form-group">
            <button class="btn" onclick="sendNotification()">Send Notification</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->