<?php

return [
    // users
    'create_users' => 'Create Users',
    'edit_users' => 'Edit Users',
    'delete_users' => 'Delete Users',
    'control_permissions' => 'Control Permissions',
    'monitor_users' => 'Monitor Users',
    'logs' => 'Users Logs',
    // visitors
    'create_visitors' => 'Create Visitors',
    'edit_visitors' => 'Edit Visitors',
    'delete_visitors' => 'Delete Visitors',
    'monitor_visitors' => 'Monitor Visitors',
    // channels
    'create_channels' => 'Create Channels',
    'edit_channels' => 'Edit Channels',
    'delete_channels' => 'Delete Channels',
    'monitor_channels' => 'Monitor Channels',
    // news
    'create_news' => 'Create News',
    'edit_news' => 'Edit News',
    'delete_news' => 'Delete News',
    'monitor_news' => 'Monitor News',
    // clips
    'create_clips' => 'Create Clips',
    'edit_clips' => 'Edit Clips',
    'delete_clips' => 'Delete Clips',
    'monitor_clips' => 'Monitor Clips',
    // templates
    'create_templates' => 'Create Templates',
    'edit_templates' => 'Edit Templates',
    'delete_templates' => 'Delete Templates',
    'monitor_templates' => 'Monitor Templates',
    // episodes
    'create_episodes' => 'Create Episodes',
    'edit_episodes' => 'Edit Episodes',
    'delete_episodes' => 'Delete Episodes',
    'monitor_episodes' => 'Monitor Episodes',
    // questions
    'create_questions' => 'Create Questions',
    'edit_questions' => 'Edit Questions',
    'delete_questions' => 'Delete Questions',
    'monitor_questions' => 'Monitor Questions',
    // schedules
    'create_schedules' => 'Create Schedules',
    'edit_schedules' => 'Edit Schedules',
    'delete_schedules' => 'Delete Schedules',
    'monitor_schedules' => 'Monitor Schedules',
    // genres
    'create_genres' => 'Create Genres',
    'edit_genres' => 'Edit Genres',
    'delete_genres' => 'Delete Genres',
    'monitor_genres' => 'Monitor Genres',
    // tags
    'create_tags' => 'Create Tags',
    'edit_tags' => 'Edit Tags',
    'delete_tags' => 'Delete Tags',
    'monitor_tags' => 'Monitor Tags',
    // shows
    'create_shows' => 'Create Shows',
    'edit_shows' => 'Edit Shows',
    'delete_shows' => 'Delete Shows',
    'monitor_shows' => 'Monitor Shows',
    // seasons
    'create_seasons' => 'Create Seasons',
    'edit_seasons' => 'Edit Seasons',
    'delete_seasons' => 'Delete Seasons',
    'monitor_seasons' => 'Monitor Seasons',
    // videos
    'create_videos' => 'Create Videos',
    'edit_videos' => 'Edit Videos',
    'delete_videos' => 'Delete Videos',
    'monitor_videos' => 'Monitor Videos',
    // presenters
    'create_presenters' => 'Create Presenters',
    'edit_presenters' => 'Edit Presenters',
    'delete_presenters' => 'Delete Presenters',
    'monitor_presenters' => 'Monitor Presenters',
    // polls
    'create_polls' => 'Create Polls',
    'edit_polls' => 'Edit Polls',
    'delete_polls' => 'Delete Polls',
    'monitor_polls' => 'Monitor Polls',
    // advertisers
    'create_advertisers' => 'Create Advertisers',
    'edit_advertisers' => 'Edit Advertisers',
    'delete_advertisers' => 'Delete Advertisers',
    'monitor_advertisers' => 'Monitor Advertisers',
    // banners
    'create_banners' => 'Create Banners',
    'edit_banners' => 'Edit Banners',
    'delete_banners' => 'Delete Banners',
    'monitor_banners' => 'Monitor Banners',
    // campaigns
    'create_campaigns' => 'Create Campaigns',
    'edit_campaigns' => 'Edit Campaigns',
    'delete_campaigns' => 'Delete Campaigns',
    'monitor_campaigns' => 'Monitor Campaigns',
    // roles
    'create_roles' => 'Create Roles',
    'edit_roles' => 'Edit Roles',
    'delete_roles' => 'Delete Roles',
    'monitor_roles' => 'Monitor Roles',
    // newsplaylists
    'create_newsplaylists' => 'Create News Playlists',
    'edit_newsplaylists' => 'Edit News Playlists',
    'delete_newsplaylists' => 'Delete News Playlists',
    'monitor_newsplaylists' => 'Monitor News Playlists',
    'order_newsplaylists' => 'Order News Playlists',
    // videoplaylists
    'create_videoplaylists' => 'Create Videos Playlists',
    'edit_videoplaylists' => 'Edit Videos Playlists',
    'delete_videoplaylists' => 'Delete Videos Playlists',
    'monitor_videoplaylists' => 'Monitor Videos Playlists',
    'order_videoplaylists' => 'Order Videos Playlists',
    // showplaylists
    'create_showplaylists' => 'Create Shows Playlists',
    'edit_showplaylists' => 'Edit Shows Playlists',
    'delete_showplaylists' => 'Delete Shows Playlists',
    'monitor_showplaylists' => 'Monitor Shows Playlists',
    'order_showplaylists' => 'Order Shows Playlists',
    // episodeplaylists
    'create_episodeplaylists' => 'Create Episodes Playlists',
    'edit_episodeplaylists' => 'Edit Episodes Playlists',
    'delete_episodeplaylists' => 'Delete Episodes Playlists',
    'monitor_episodeplaylists' => 'Monitor Episodes Playlists',
    'order_episodeplaylists' => 'Order Episodes Playlists',
    // clients
    'create_clients' => 'Create Candidates',
    'edit_clients' => 'Edit Candidates',
    'delete_clients' => 'Delete Candidates',
    'control_exams' => 'Control Exams',
    'control_clients' => 'Control All Candidates',
    'monitor_clients' => 'Monitor Candidates',
    //categories
    'create_categories' => 'Create Categories',
    'edit_categories' => 'Edit Categories',
    'delete_categories' => 'Delete Categories',
    'monitor_categories' => 'Monitor Categories',
    // assessments
    'create_assessments' => 'Create Assessments',
    'edit_assessments' => 'Edit Assessments',
    'delete_assessments' => 'Delete Assessments',
    'monitor_assessments' => 'Monitor Assessments',
    // media
    'create_media' => 'Create Media',
    'edit_media' => 'Edit Media',
    'delete_media' => 'Delete Media',
    'monitor_media' => 'Monitor Media',
    // settings
    'edit_settings' => 'Edit Settings',
    'monitor_settings' => 'Monitor Settings',
    'social_auth' => 'Social Authentication',
    'statistics_analytics' => 'Statistics & Analytics',
    'push_notifications' => 'Push Notifications',
    'manage_reports' => 'Manage Reports',
    // reports
    'show_report' => 'Show Reports',
    'export_pdf' => 'Export PDF'
];
