<?php

return [
    "fill_out" => "Fill-out Forms and Input fields",
    'field_required' => 'This field is required.',
    'email_valid' => 'Please enter a valid email address.',
    'equal_to' => 'Please enter the same value again.',
    'same_value' => 'Please enter the same value again.',
    'ext_valid' => 'Please enter a value with a valid extension.',
    'home' => 'Home',
    'some_errors' => 'You have some form errors. Please check below.',
];
