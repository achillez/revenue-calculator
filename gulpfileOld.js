var elixir = require('laravel-elixir');
/*var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');*/

// Configure elixir to use different directories

// elixir.config.assetsDir = 'app/assets/';
// elixir.config.cssOutput = 'public_html/css';
// elixir.config.jsOutput = 'public/js';
// elixir.config.bowerDir = 'vendor/bower_components';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.styles([
        "global/plugins/font-awesome/css/font-awesome.min.css",
        "global/plugins/simple-line-icons/simple-line-icons.min.css",
        "global/plugins/bootstrap/css/bootstrap.min.css",
        "global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
        "global/css/components.min.css",
        "global/css/plugins.min.css",
        "layouts/layout/css/layout.min.css",
        "layouts/layout/css/themes/darkblue.min.css",
        "layouts/layout/css/custom.min.css"
    ], 'public/assets/app.css', 'public/assets')
            .scripts([
                "global/plugins/jquery.min.js",
                "global/plugins/bootstrap/js/bootstrap.min.js",
                "global/plugins/js.cookie.min.js",
                "global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
                "global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                "global/plugins/jquery.blockui.min.js",
                "global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
                "global/scripts/app.min.js",
                "layouts/layout/scripts/layout.min.js",
                "layouts/layout/scripts/demo.min.js",
                "layouts/global/scripts/quick-sidebar.min.js",
            ], 'public/assets/app.js', 'public/assets')
            .version(['assets/app.css', 'assets/app.js']);
});

/*gulp.task('compress', function () {
    var opts = {
        collapseWhitespace: true,
        removeAttributeQuotes: true,
        removeComments: true,
        minifyJS: true
    };*/

    //return gulp.src('./storage/framework/views/**/*')
            //.pipe(htmlmin(opts))
            //.pipe(gulp.dest('./storage/framework/views/'));
//});