<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // constants
        require_once __DIR__ . '/../Http/constants.php';

        // helpers
        require_once __DIR__ . '/../Http/helpers.php';

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

}
