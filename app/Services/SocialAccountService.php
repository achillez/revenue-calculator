<?php

namespace App\Services;

use App\SocialAccount;
use App\Visitor;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialAccountService
{

    public function createOrGetUser(ProviderUser $providerUser, $provider)
    {
        $account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if (isset($account->user)) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider
            ]);

            $user = Visitor::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $user = new Visitor;
                $user->email = $providerUser->getEmail() ? $providerUser->getEmail() : create_slug($providerUser->getName()) . '@facebook.com';
                $user->fullname = $providerUser->getName();
                $user->avatar = $providerUser->getAvatar();
                $user->api_token = str_random(60);
                $user->save();
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }

}
