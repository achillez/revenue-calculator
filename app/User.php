<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyOwnResetPassword as ResetPasswordNotification;
use NotificationChannels\WebPush\HasPushSubscriptions;

class User extends Authenticatable
{

    use Notifiable;
    use HasPushSubscriptions;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token, ADMINURL, $this->email));
    }

    // check is admin user
    public function isAdmin()
    {
        return in_array(session('priority'), config('system.admins'));
    }

    public function getNameAttribute()
    {
        return $this->fullname;
    }

    public function getGroupNameAttribute()
    {
        return ($this->role) ? $this->role->name : FALSE;
    }

    public function getAvatarUrlAttribute()
    {
        return $this->avatar ? url($this->avatar) : url('media/user.svg');
    }

    public function getPermissionsAttribute($value)
    {
        return (array)json_decode($value);
    }

    // substitute your list of fields you want to be auto-converted to timestamps here:
    public function getDates()
    {
        return array('created_at', 'updated_at', 'last_login');
    }

//    public function getGroupAttribute() {
//
//        return ($this->role()) ? $this->role()->first() : FALSE;
//    }

    public function scopeValidRole($query)
    {
        // check priority
        $priority = session('priority');

        if (!in_array($priority, config('system.admins'))) {

            $query->whereHas('role', function ($query) use ($priority) {
                $query->where('priority', '>', $priority);
            });
        }

        return $query;
    }

    public static function allow($params = array())
    {
        $params = is_array($params) ? $params : func_get_args();
        $permissions = session("permissions");
        if (!count($permissions))
            return false;
        $permissions_string = join("", $permissions);
        if (count($params)) {
            foreach ($params as $param) {
                if (!in_array($param, $permissions)) {
                    if (!strstr($permissions_string, $param . ".")) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    // relations
    public function role()
    {
        return $this->belongsToMany('App\Role', 'users_roles', 'user_id', 'role_id');
    }

    public function permissions()
    {
        return $this->hasMany('App\UserPermission', 'user_id');
    }

    public function logs()
    {
        return $this->hasMany('App\UserLogs', 'user_id');
    }

    public static function boot()
    {

        parent::boot();

        User::updated(function ($model) { // before delete() method call this
            if (auth()->user()->id != $model->id) {
                $log = new UserLogs(['model_name' => 'users', 'model_id' => $model->id, 'action_name' => 'update', 'user_id' => auth()->user()->id]);
                auth()->user()->logs()->save($log);

                if ($model->attributes['activated'] != $model->original['activated'] && $model->attributes['activated'] != 1) {
                    \DB::table('sessions')->where('user_id', $model->id)->delete();
                }
            }
        });

        User::created(function ($model) { // before delete() method call this
            $log = new UserLogs(['model_name' => 'users', 'model_id' => $model->id, 'action_name' => 'create', 'user_id' => auth()->user()->id]);
            auth()->user()->logs()->save($log);
        });

        User::deleting(function ($model) { // before delete() method call this
            $log = new UserLogs(['model_name' => 'users', 'model_id' => $model->id, 'action_name' => 'delete', 'user_id' => auth()->user()->id]);
            auth()->user()->logs()->save($log);

            $model->role()->sync([]);
            // do the rest of the cleanup...
        });
    }

}
