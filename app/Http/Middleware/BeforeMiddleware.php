<?php

namespace App\Http\Middleware;

use Closure;

class BeforeMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        // enable query logging
//        \DB::enableQueryLog();

        // check if user is not active
        if (auth()->check() && !auth()->guard('visitor')->check()) {
            if (!auth()->user()->activated) {
                auth()->logout();
                return redirect(ADMINURL . '/login');
            }
        }

        return $next($request);
    }

}
