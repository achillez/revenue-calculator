<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = ADMINURL . '/';
    protected $username = 'username';
    protected $redirectAfterLogout = ADMINURL . '/login';
    protected $guard = null;
    protected $ADMINURL = ADMINURL;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request) {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        // check recaptcha
        $validcode = $this->validRecaptcha($request);
        if (!$validcode) {
            return redirect()->back()
                            ->withErrors(['g-recaptcha-response' => trans("auth.recaptcha")])
                            ->withInput($request->except('password'));
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of assessment, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if (!$lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request) {
        $username = $this->username();
        return [$username => $request->$username, 'password' => $request->password, 'activated' => 1];
    }

    /**
     * Validate recaptcha
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private function validRecaptcha(Request $request) {
        require_once(app_path() . "/recaptcha/ReCaptcha.php");
        // your secret key
        $secret = "6Lcd4CQTAAAAAH5HoBprYdm08IUJFTDmzfTqI6Uy";

        // empty response
        $response = null;

        // check secret key
        $reCaptcha = new \ReCaptcha($secret);
        // if submitted check response
        if ($request->has('g-recaptcha-response')) {
            $response = $reCaptcha->verifyResponse($request->ip, $request->get('g-recaptcha-response'));
        }

        if ($response == null || !$response->success) {
            return false;
        }

        return true;
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request) {
        $this->validate($request, [
            $this->username() => 'required', 'password' => 'required', 'g-recaptcha-response' => 'required'
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username() {
        return $this->username;
    }

    /**
     * Log the user out of the application.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect($this->redirectAfterLogout);
    }

}
