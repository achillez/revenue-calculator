<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServersRequest;
use Illuminate\Http\Request;
use Redirect;

class HomeController extends Controller
{
    public $data = array();

    public function __construct(Request $request)
    {

    }

    public function index(Request $request)
    {

//        Location::create([
//                'user_ip' => determineIP() //$request->getClientIp()
//            ]
//        );

        return view('index')->with($this->data);
    }
}
