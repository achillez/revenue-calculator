<?php

namespace App\Policies;

class UserPolicy
{
    /**
     * Determine if the given post can be updated by the user.
     *
     * @param  \App\User $user
     * @return bool
     */
    public function create($user)
    {
        return $user->allow("users.create");
    }

    public function update($user, $user2)
    {
        return $user->allow("users.edit") || $user->allow("users.monitor") || $user2->id == $user->id;
    }

    public function edit($user, $user2)
    {
        return $user->allow("users.edit") || $user2->id == $user->id;
    }

    public function delete($user)
    {
        return $user->allow("users.delete");
    }

    public function monitor($user)
    {
        return $user->allow("users");
    }

    public function logs($user)
    {
        return $user->allow("users.logs");
    }

    public function permissions($user)
    {
        return $user->allow("users.permissions");
    }
}