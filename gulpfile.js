var gulp = require('gulp');
var minifyCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');

gulp.task('css', function(){
  return gulp.src('client/style/*.css')
	.pipe(concat('style.min.css'))
	.pipe(minifyCSS())
    .pipe(gulp.dest('build/css'))
});

gulp.task('js', function(){
  return gulp.src('client/javascript/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('app.min.js'))
    .pipe(sourcemaps.write())
	.pipe(uglify())
    .pipe(gulp.dest('build/js'))
});

gulp.task('default', ['css', 'js' ]);